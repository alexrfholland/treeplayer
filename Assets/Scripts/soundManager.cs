﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class soundManager : MonoBehaviour
{
    public static bool isReset = false;

    public Color32 otherCol;

    Color32 hueColour;
    Color32 colour;
    [Range(0,255)]
    public byte trans;

    AudioSource audio;
    public AudioSource[] speakerPool;
    public AudioClip[] soundClips;
    static AudioClip[] _soundClips;

    public int nPlayed;
    public int ntoPlay;

    public Text ErrorText;
    public Text totalText;

    string treeList;
    string totalTreeList;
    string _totalTreeList;
    string hexColour;

    public string[] treeTextBuffer = new string[30];

    static List<TreeClass> treeQueue = new List<TreeClass>();
   public  static List<TreeClass> playedTreeQueue = new List<TreeClass>();
    public static List<TreeDot> visTreeGrain = new List<TreeDot>();
    static List<AudioClip> clipQueue = new List<AudioClip>();
    static List<int> settingsID = new List<int>();
    public List<int> _settingsID = new List<int>();

    public AudioMixerGroup[] Mixers;

    static bool isPlayingOther = false;

    // Use this for initialization
    void Start()
    {
        _soundClips = soundClips;
        _settingsID = settingsID;
    }

    // Update is called once per frame
    void Update()
    {
        if (isReset)
        {
            totalTreeList = "/n";
            isReset = false;
        }

        nPlayed = playedTreeQueue.Count;
        ntoPlay = treeQueue.Count;
        if (ErrorText != null)
        {
            ErrorText.text = treeList;
        }
         UpdateSpeakers();

        totalText.text = totalTreeList;
    }
    




    IEnumerator stopTree(TreeClass tree, float delay)
    {
        yield return new WaitForSeconds(tree.soundClip.length + delay);
        tree.isPlaying = false;

        if (tree.soundIndex == 25)
        {
            soundManager.isPlayingOther = false;
        }
    }

    void treeText(TreeClass tree)
    {
        for (int i = System.Math.Max(visTreeGrain.Count - 30, 0); i < visTreeGrain.Count; i++)
        {


            hueColour = Color.HSVToRGB(visTreeGrain[i].tree.commonVal, 1, 1);
            colour = new Color32(hueColour.r, hueColour.g, hueColour.b, (byte)trans);

            hexColour = ColorUtility.ToHtmlStringRGBA(colour).ToString();

            int mI = i - System.Math.Max(visTreeGrain.Count - 30, 0);

            treeTextBuffer[mI] = "<color=#" + hexColour + ">" + visTreeGrain[i].tree.treeInfo[4] + "</color>" + visTreeGrain[i].bLI + "," + visTreeGrain[i].tLI + " " + (TreeDot.bucketList.Count)+ "\n";

           
        }

        totalTreeList = string.Concat(treeTextBuffer);

        hueColour = Color.HSVToRGB(tree.commonVal, 1, 1);
        colour = new Color32(hueColour.r, hueColour.g, hueColour.b, (byte)trans);


        hexColour = ColorUtility.ToHtmlStringRGBA(colour).ToString();

        treeList = "<color=#" + hexColour + ">" + tree.treeInfo[4] + "</color>";
    }

    IEnumerator startTree(TreeClass tree, float delay)
    {
       

        yield return new WaitForSeconds( delay);

        
        hueColour = Color.HSVToRGB(tree.commonVal, 1, 1);
        colour = new Color32(hueColour.r, hueColour.g, hueColour.b, (byte)trans);

       
        hexColour = ColorUtility.ToHtmlStringRGBA(colour).ToString();



        treeList = "<color=#" + hexColour + ">" + tree.treeInfo[4] + "</color>";

       
        totalTreeList= totalTreeList + "<color=#" + hexColour + ">" + tree.treeInfo[4] + "</color>" + "\n";




        // totalTreeList = totalTreeList.Remove(0, index);




        /*if (tree.soundIndex < 25)
        {
            tree.isPlaying = true;
            //treeList = "<color=#"+otherString+">"+treeList+"</color>" + "<color=#"+hexColour+">"+tree.treeInfo[4]+"</color> \n";
            treeList = "<color=#" + hexColour + ">" + tree.treeInfo[4] + "</color>";
            //treeList = treeList + "<b>" + tree.treeInfo[4] + "</color>" + "</b>" + "\n";





        }
        else
        {
            treeList = treeList + "<size=100>" +  tree.treeInfo[4] + "</size>" + "\n";
        }
        */
    }


    void UpdateSpeakers()
    {
        float delay;

        //cutDownTreesInQueThatAreFarFarAway();
        for (int i = 0; i < speakerPool.Length; i++)
        {
            if ((Vector3.SqrMagnitude(speakerPool[i].transform.position - Data.playerPosition) > 20 * 20 || !speakerPool[i].isPlaying) && treeQueue.Count > 0) // if you have a clip to play in treeQueue and there is a speaker available
            {
                speakerPool[i].transform.position = treeQueue[0].position;


                treeQueue[0].attachedSource = speakerPool[i];
                speakerPool[i].clip = _soundClips[treeQueue[0].soundIndex];
                clipSettings(treeQueue[0], speakerPool[i]);

                delay = 0; // ((float)(Random.Range(0, 500)) / 100);

                speakerPool[i].PlayDelayed(delay);

                /* if (treeQueue[0].soundIndex < 25 || (!isPlayingOther))
                {
                    if (treeQueue[0].soundIndex == 25)
                    {
                        isPlayingOther = true;
                        
                    }
            
                    speakerPool[i].PlayDelayed(delay);
                }
                */

               // StartCoroutine(stopTree(treeQueue[0], delay));
               //s StartCoroutine(startTree(treeQueue[0], delay));

                treeText(treeQueue[0]);
                

                playedTreeQueue.Add(treeQueue[0]);
                //   if!(allPlayed.Contains(treeQueue[0])
                visTreeGrain.Add(new TreeDot(treeQueue[0], Time.time, i));
                treeQueue.RemoveAt(0);
               // yield return new WaitForSeconds(delay);

            }
        }
    }

    void cutDownTreesInQueThatAreFarFarAway()
    {
      //  for (int j = 0; j < treeQueue.Count; j++)
      //  {
            for (int i = 0; i < treeQueue.Count; i++)
            {
                if (Vector3.SqrMagnitude(treeQueue[i].position - Data.playerPosition) > 500 * 500)
                {
                  //  playedTreeQueue.Add(treeQueue[i]);
                    treeQueue.RemoveAt(i);
                    break;
                }
            }
      //  }
    }

    void clipSettings(TreeClass thisTree, AudioSource speaker)

    { 
        switch (thisTree.soundIndex)
        {
            case 0:
                speaker.volume = 1f;
         //       speaker.pitch = 2;
                break;
            case 1:
                speaker.volume = 1f;
                break;

            default:
                speaker.volume = 1;
             //   speaker.pitch = 0.2f;
                break;
        }
        //speaker.pitch = 3f;
    }

    public static void clearQueue(){
        clipQueue.Clear();
        settingsID.Clear();

        treeQueue.Clear();
        playedTreeQueue.Clear();
    }

    public static void addTreeSoundToQueue(TreeClass tree)
    {


        //////////////////////////////////// ignore

        int soundIndex;
        int age;
        int ageIndex;
        if (int.TryParse(tree.treeInfo[9], out age))
        {
            age = 2017 - age;
        }

        else
        {
            age = 1;
        }

        tree.age = age;

        if (age < 20)
        {
            ageIndex = 0;
        }

        else if (age < 50)
        {
            ageIndex = 1;
        }

        else
        {
            ageIndex = 2;
        }



        switch (ageIndex)
        {
            case 0:
                settingsID.Add(0);
                break;

            case 1:
                settingsID.Add(1);
                break;

            default:
                settingsID.Add(2);
                break;
        }
        soundIndex = -1;
        //treeInfo[7 is family, 6 is Genus, 5 is Scientific, 4 is Common Name
        //
        if (tree.treeInfo[4] == Data.CommonNames[20])
        {
            //Spotted Gum
            //MCS [2]

            soundIndex = 2;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[33])
        {
            //Lemon Scented Gum
            //MCL [3]
            soundIndex = 3;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[213])
        {
            //Grey Gum
            //MEG [5]
            soundIndex = 5;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[1])
        {
            //Red River Gum
            // MER [6]
            soundIndex = 6;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[126])
        {
            //Swamp Gum
            //MES [7]
            soundIndex = 7;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[7])
        {
            //Yellow Box
            //MEYB [8]
            soundIndex = 8;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[14])
        {
            //Yellow Gum
            // MEYG [9]
            soundIndex = 9;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[301])
        {
            //Horizontal Elm
            // UGL [12]
            soundIndex = 12;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[159])
        {
            //Dutch Elm
            // UXH [13]
            soundIndex = 13;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[61])
        {
            //Chinese Elm
            //UPAR [14]
            soundIndex = 14;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[26])
        {
            //English Elm
            //UPRO [15]
            soundIndex = 15;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[191])
        {
            //Smooth-Leave
            //UMI [16]
            soundIndex = 16;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[24])
        {
            //Elm
            //USP [17]
            soundIndex = 17;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[10])
        {
            //Silky Oak
            //PGS [19]
            soundIndex = 19;

        }
        else if (tree.treeInfo[4] == Data.CommonNames[130])
        {
            //Yellow Chestnut Oak
            // FQY [20]
            soundIndex = 20;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[156])
        {
            //Black She Oak
            //CAB [21]
            soundIndex = 21;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[5])
        {
            //London Plane
            //PPL [22]
            soundIndex = 22;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[9])
        {
            //Cyprus Plane
            //PPC [23]
            soundIndex = 23;
        }
        else if (tree.treeInfo[4] == Data.CommonNames[211])
        {
            //Autumn Glory Plane
            //PPA [24]
        }
        else if (tree.treeInfo[6] == Data.Genus[31])
        {
            //Angophora
            //MA [0]
            soundIndex = 0;

        }
        else if (tree.treeInfo[6] == Data.Genus[13])
        {
            //Corymbia
            //MC [1]
            soundIndex = 1;

        }
        else if (tree.treeInfo[6] == Data.Genus[1])
        {
            //Eucalyptus
            //ME [4]
            soundIndex = 4;

        }
        else if (tree.treeInfo[6] == Data.Genus[12])
        {
            //Melaleuca
            //MM [10]
            soundIndex = 10;
        }
        else if (tree.treeInfo[7] == Data.Family[1] || tree.treeInfo[7] == Data.Family[31])
        {
            //Myrtaceae
            //MO [11]
            soundIndex = 11;
        }
        else if (tree.treeInfo[7] == Data.Family[10])
        {
            //Ulmaceae
            //UO [18]
            soundIndex = 18;
        }
        else
        {
            soundIndex = 25;// All Other Trees
        }
       //////////////////////////////////////////



        if (soundIndex >= 0 && soundIndex <= 25)
        {
            tree.soundIndex = soundIndex;
            tree.soundClip = _soundClips[soundIndex];
            treeQueue.Add(tree);
        }
        /* 
        else if (tree.treeInfo[7] == Data.Family[2]) {
                //Fagaceae
            }else if (tree.treeInfo[7] == Data.Family[9]) {
                //Casuarinaceae
            }else if (tree.treeInfo[7] == Data.Family[40]) {
                //Platanus
                }
        //}else if (tree.treeInfo[6] == Data.Genus[14])
        //{
            //Ulmus

        }else if (tree.treeInfo[7] == Data.Family[6]) {
                //Proteaceae
            }else if (tree.treeInfo[6] == Data.Genus[6])
        {
            //Grevillea

        }else if (tree.treeInfo[6] == Data.Genus[2])
        {
            //Quercus
        }else if (tree.treeInfo[6] == Data.Genus[9])
        {
            //Allocasuarina
        }else if (tree.treeInfo[6] == Data.Genus[4])
        {
            //Platanus
        }else{
            //Other
            */

    }

 


}
