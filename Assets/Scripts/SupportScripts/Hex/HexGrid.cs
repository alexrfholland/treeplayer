﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HexGrid
{

    public int gridXLength, gridZLength, indexLength;
    public Vector3 GridOrigin;
    public List<HexCell> cells;
    public Vector3[] cellCentres;

    public HexGrid(List<double[]> CSVHexInfo, float OuterCellRadius)
    {
        indexLength = (int)CSVHexInfo.Max(x => x[5]) + 1;
        gridXLength = (int)CSVHexInfo.Max(x => x[4]) + 1;
        gridZLength = (int)CSVHexInfo.Max(x => x[3]) + 1;

        GridOrigin = GPSManager.getUnityPosition(CSVHexInfo[0][1], CSVHexInfo[0][0]);
        cellCentres = new Vector3[indexLength];

        HexCell[] tempcells = new HexCell[indexLength];
        Vector3 CellPosition;
        HexCoordinates.Grid = this;
        int index;
        for (int i = 0; i < indexLength; i++)
        {
            index = (int)(CSVHexInfo[i][5]);
            CellPosition = GPSManager.getUnityPosition(CSVHexInfo[i][1], CSVHexInfo[i][0]);
            tempcells[index] = new HexCell(CellPosition, CSVHexInfo[i][6], CSVHexInfo[i][7], CSVHexInfo[i][8]);
            cellCentres[index] = CellPosition;
        }

        cells = new List<HexCell>(tempcells);

    }

    /// <summary>
    /// This will return a list of the cell centres given a list of indecies.
    /// </summary>
    /// <param name="indicies"></param>
    /// <returns></returns>
    public Vector3[] getPositions(int[] indicies)
    {
        Vector3[] listOut = new Vector3[indicies.Length];
        for (int i = 0; i < indicies.Length; i++)
        {
            listOut[i] = cellCentres[indicies[i]];
        }
        return listOut;
    }
}
