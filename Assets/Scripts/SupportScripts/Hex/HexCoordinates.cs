﻿using UnityEngine;



public enum HexDirection { SE, NE, N, NW, SW, S }

public static class enumExtensions
{
    public static HexDirection Opposite(this HexDirection direction)
    {
        return (HexDirection)(((int)direction + 3) > 6 ? ((int)direction - 3) : ((int)direction + 3));

    }
    public static int[] indexArray
    {
        get
        {
            int[] output = new int[System.Enum.GetNames(typeof(HexDirection)).Length];
            for (int i = 0; i < output.Length; i++)
            {
                output[i] = i;
            }
            return output;
        }
    }
}

[System.Serializable]
public struct HexCoordinates
{
    [SerializeField]
    private int x, z;
    public static HexGrid Grid;

    public int X
    {
        get
        {
            return x;
        }
    }

    public int Z
    {
        get
        {
            return z;
        }
    }

    public int Y
    {
        get
        {
            return -X - Z;
        }
    }

    /// <summary>
    /// Create a new HexCoordinate with X,Z hex coordinates, not rectangular coordinates!
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    public HexCoordinates(int x, int z)
    {
        this.x = x;
        this.z = z;
    }

    
    /// <summary>
    /// Returns a String "(X,Z,Y)"
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        return "(" + X.ToString() + ", " + Z.ToString() + ", " + Y.ToString() + ")";
    }

    public string ToStringOnSeparateLines()
    {
        return X.ToString() + "\n" + Z.ToString();
    }

    /// <summary>
    /// Get the HexCoordinate using rectangular X,Z coordiantes. This will work independently of any grid.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    public static HexCoordinates hFr(int x, int z)
    {
        return new HexCoordinates( x, z - x / 2);
    }

    

    /// <summary>
    /// Get the HexCoordinate from the indicie value of the 1D array of HexCells in the CAmanager and CSV intensity maps.
    /// This relies on the size of a HexGrid defined in CSV.gridZLength and CSV.gridXLength from the first CSV loaded
    /// will return (int.MinValue,int.MinValue) if out of bounds
    /// </summary>
    /// <param name="i"></param> This indicie of the cell within the HexCAGrid
    /// <returns></returns>
    public static HexCoordinates hFi(int i)
    {
        if (i >= 0 && i < Grid.indexLength)
        {
            return new HexCoordinates((i / Grid.gridZLength), (i % Grid.gridZLength) - (i / Grid.gridZLength) / 2);
        }
        else
        {
            return new HexCoordinates(int.MinValue, int.MinValue);
        }
    }


    /// <summary>
    /// Get the HexCoordinate that is within the Vector3 Position. This will work independently of any grid.
    /// This relies on the size of a hexCell defined in 'HexMetrics.innerRadius' and 'HexMetrics.outerRadius'
    /// </summary>
    /// <param name="position"></param> The Vector3 position relative to origin which will be within a HexCoordinate.
    /// <returns></returns>
    public static HexCoordinates hFp(Vector3 position)
    {
        position = position -Grid.GridOrigin;

        float z = position.z / (HexMetrics.InnerRadius * 2f);
        float y = -z; 
        float offset = position.x / (HexMetrics.OuterRadius * 3f);
        z -= offset;
        y -= offset;
        int iZ = Mathf.RoundToInt(z);
        int iY = Mathf.RoundToInt(y);
        int iX = Mathf.RoundToInt(-z - y);
        if (iX + iY + iZ != 0)
        {
            float dZ = Mathf.Abs(z - iZ);
            float dY = Mathf.Abs(y - iY);
            float dX = Mathf.Abs(-z - y - iX);

            if (dZ > dY && dZ > dX)
            {
                iZ = -iY - iX;
            }
            else if (dX > dY)
            {
                iX = -iZ - iY;
            }
        }
        return new HexCoordinates(iX, iZ);
    }

    /// <summary>
    /// Get the indicie number of the 1D array of HexCells in the HexCAGrid from rectangular X,Z locations.
    /// This relies on the size of a HexGrid defined in CSV.gridZLength and CSV.gridXLength from the first CSV loaded
    /// -1 will return if the X,Z location is out of the grid bounds.
    /// </summary>
    /// <param name="x"></param> This is offset coordinates, X locations, of a 2D grid of hex cells where max width is 'HexCAGrid.width'
    /// <param name="z"></param> This is offset coordinates, Z locations, of a 2D grid of hex cells where max height is 'HexCAGrid.height'
    /// <returns></returns>
    public static int iFr(int x, int z)
    {
        if (x >= 0 && x < Grid.gridXLength && z >= 0 && z < Grid.gridZLength)
        {
            return (z + Grid.gridZLength * x);
        }
        else
        {
            return -1;
        }
    }


    /// <summary>
    /// Get the indicie number of the 1D array of HexCells in the HexCAGrid from Hex coordinates.
    /// This relies on the size of a HexGrid defined in CSV.gridZLength and CSV.gridXLength from the first CSV loaded
    /// -1 will return if the X,Z location is out of the grid bounds.
    /// </summary>
    /// <param name="hex"></param> The Hex Coordinate to convert to an indicie within HexCAGrid
    /// <returns></returns>
    public static int iFh(HexCoordinates hex)
    {

        if (hex.X >= 0 && hex.X < Grid.gridXLength && (hex.Z + hex.X / 2) >= 0 && (hex.Z + hex.X / 2) < Grid.gridZLength)
        {
            return Grid.gridZLength * hex.X + hex.Z + hex.X / 2;
        }
        else
        {
            return -1;
        }
    }


    /// <summary>
    /// Get the indicie number of the 1D array of HexCells in the HexCAGrid from a Vector3 Position.
    /// This relies on the size of a HexGrid defined in CSV.gridZLength and CSV.gridXLength from the first CSV loaded
    /// -1 will return if the X,Z location is out of the grid bounds.
    /// </summary>
    /// <param name="position"></param> The Vector3 Position to convert to an indicie within HexCAGrid
    /// <returns></returns>
    public static int iFp(Vector3 position)
    {
        position = position - Grid.GridOrigin;

        float z = position.z / (HexMetrics.InnerRadius * 2f);
        float y = -z;
        float offset = position.x / (HexMetrics.OuterRadius * 3f);
        z -= offset;
        y -= offset;
        int iZ = Mathf.RoundToInt(z);
        int iY = Mathf.RoundToInt(y);
        int iX = Mathf.RoundToInt(-z - y);
        if (iX + iY + iZ != 0)
        {
            float dZ = Mathf.Abs(z - iZ);
            float dY = Mathf.Abs(y - iY);
            float dX = Mathf.Abs(-z - y - iX);

            if (dZ > dY && dZ > dX)
            {
                iZ = -iY - iX;
            }
            else if (dX > dY)
            {
                iX = -iZ - iY;
            }
        }
        if (iX >= 0 && iX < Grid.gridXLength && (iZ + iX / 2) >= 0 && (iZ + iX / 2 )< Grid.gridZLength)
        {
            return Grid.gridZLength * iX + iZ + iX / 2;
        }
        else
        {
            return -1;
        }
    }



    //////////////////////////////////////////////////////
    ///////////    Not done below yet
    //////////////////////////////////////////////////////

    /// <summary>
    /// Get the vector3 Position of the hex from rectangular X,Z locations. This will work independently of any grid.
    /// This relies on the size of a hexCell defined in 'HexMetrics.innerRadius' and 'HexMetrics.outerRadius'
    /// </summary>
    /// <param name="x"></param> This is using offset coordinates where the size of the cell width is determined by 'HexMetrics.innerRadius'
    /// <param name="z"></param> This is using offset coordinates where the size of the cell height is determined by 'HexMetrics.outerRadius'
    /// <returns></returns>
    public static Vector3 pFr(int x, int z)
    {
        Vector3 position;
        position.z = (z + x * 0.5f - x / 2) * (HexMetrics.InnerRadius * 2f);
        position.y = 0f;
        position.x = x * (HexMetrics.OuterRadius * 1.5f);
        return Grid.GridOrigin + position;
    }




    /// <summary>
    /// Gets the vector3 Position of the HexCell within the 1D list of cells in HexCAGrid. 
    /// This relies on the size of a hexCell defined in 'HexMetrics.innerRadius' and 'HexMetrics.outerRadius'
    /// This relies on the size of a HexGrid defined in CSV.gridZLength and CSV.gridXLength from the first CSV loaded
    /// (NaN,NaN,NaN) will be returned if out of bounds
    /// </summary>
    /// <param name="i"></param> The indicie value in the 1D list of HexCells within HexCAGrid.
    /// <returns></returns>
    public static Vector3 pFi(int i)
    {
        if (i >= 0 && i < Grid.indexLength)
        {
            Vector3 position;
            position.z = (i % Grid.gridZLength + i / Grid.gridZLength * 0.5f - i / Grid.gridZLength / 2) * (HexMetrics.InnerRadius * 2f);
            position.y = 0f;
            position.x = i / Grid.gridZLength * (HexMetrics.OuterRadius * 1.5f);
            return Grid.GridOrigin + position;
        }
        else
        {
            return new Vector3(float.NaN, float.NaN, float.NaN);
        }
    }



    /// <summary>
    /// Gets the vector3 Postiion of the HexCell from Hex Coordinates.  This will work independently of any grid.
    /// This relies on the size of a hexCell defined in 'HexMetrics.innerRadius' and 'HexMetrics.outerRadius'
    /// </summary>
    /// <param name="hex"></param> The HexCoordinate which is converted.
    /// <returns></returns>
    public static Vector3 pFh(HexCoordinates hex)
    {
        Vector3 position;
        position.z = (hex.Z + hex.x * 0.5f) * (HexMetrics.InnerRadius * 2f);
        position.y = 0f;
        position.x = hex.x * (HexMetrics.OuterRadius * 1.5f);
        return Grid.GridOrigin + position;
    }

    /// <summary>
    /// Gets the rectangular X,Z locations as a Vector2. 
    /// (int)Vector2.x is the X index
    /// (int)Vector2.y is the Z index 
    /// This relies on the size of a HexGrid defined in CSV.gridZLength and CSV.gridXLength from the first CSV loaded
    /// (NaN,NaN) will be returned if out of bounds
    /// </summary>
    /// <param name="i"></param>
    /// <returns></returns>
    public static Vector2 rFi(int i)
    {
        if (i >= 0 && i < Grid.indexLength)
        {
            return new Vector2(i / Grid.gridZLength, i % Grid.gridZLength);
        }
        return new Vector2(float.NaN,float.NaN);
    }

    /// <summary>
    /// Gets the rectangular X,Z locations as a Vector2. This will work independently of any grid.
    /// (int)Vector2.x is the X index
    /// (int)Vector2.y is the Z index 
    /// </summary>
    /// <param name="hex"></param>
    /// <returns></returns>
    public static Vector2 rFh(HexCoordinates hex)
    {
        return new Vector2(hex.x, hex.z + hex.x / 2);
    }

    /// <summary>
    /// Gets the rectangular X,Z locations as a Vector2. This will work independently of any grid.
    /// (int)Vector2.x is the X index
    /// (int)Vector2.y is the Z index 
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public static Vector2 rFp(Vector3 position)
    {
        return rFh(hFp(position));
    }

    

}


/// <summary>
/// This defines the size of a hexCell. This is used when converting Vector3 positions to Hex / zigzag and indicie coordinates.
/// </summary>
public static class HexMetrics
{
    public const float RadiusChange = 0.866025404f;
    static float outerRadius = 1f;

    static float innerRadius = outerRadius * RadiusChange;

    public static Vector3[] corners = {
        new Vector3( 0.5f * outerRadius, 0f,-innerRadius),
        new Vector3(outerRadius, 0f, 0f),
        new Vector3( 0.5f * outerRadius, 0f,innerRadius),
        new Vector3(-0.5f * outerRadius, 0f,innerRadius),
        new Vector3( -outerRadius,0f, 0f),
        new Vector3( -0.5f * outerRadius, 0f,-innerRadius),
        new Vector3( 0.5f * outerRadius, 0f,-innerRadius)
    };

    public static float cellRadius
    {
        get
        {
            return outerRadius;
        }
        set
        {
            outerRadius = value;
            innerRadius = outerRadius * RadiusChange;
        }
    }

    public static float InnerRadius { get { return innerRadius; } }
    public static float OuterRadius { get { return outerRadius; } }
}
