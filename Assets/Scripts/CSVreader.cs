﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.IO;
using System.Threading;
using System.ComponentModel;

public class CSVreader
{
    // Object files, once read this will store the information.
    List<double[]>[] CSVdata;
    List<string>[] Columns;
    List<string[]>[] CSVtext;

    public List<double[]>[] CSVData { get { return CSVdata; } }
    public List<string>[] ColumnNames { get { return Columns; } }
    public List<string[]>[] CSVText{ get { return CSVtext; } }

    public CSVreader(TextAsset[] CSVData)
    {
        readCSVfiles(CSVData, out Columns, out CSVdata, out CSVtext);
        
    }

    public CSVreader(TextAsset CSVData)
    {
        readCSVfiles(new TextAsset[] { CSVData }, out Columns, out CSVdata, out CSVtext);
    }

    /// <summary>
    /// Will read a list of CSV text files, If the first row has less columns then it will assume it is settings saving it in a dictionary.
    /// </summary>
    /// <param name="CSVText"></param>
    /// <param name="ColumnNames"></param>
    /// <param name="CSVnumbers"></param> A list of a double array, where each element of the list is a row of that CSV. As there is a stack of CSV files you access the CSV then the row then the column.
    /// <returns></returns>
    void readCSVfiles(TextAsset[] CSVText, out List<string>[] ColumnNames, out List<double[]>[] CSVnumbers, out List<string[]>[] CSVtext)
    {
        // CSV text settings and control
        string ret = "\r";
        char rowChar = '\n';
        char columnChar = ',';

        int numColumns;

        CSVnumbers = new List<double[]>[CSVText.Length];
        ColumnNames = new List<string>[CSVText.Length];
        CSVtext = new List<string[]>[CSVText.Length];

        string[] CSVsplitToLines;
        string[] AlineSplitToColums;

        double ReadValue;
        // For each intensity map we need to tell the computer we will have a list of points (vector3) and data values
        for (int j = 0; j < CSVText.Length; j++)
        {
            CSVnumbers[j] = new List<double[]>();
            CSVtext[j] = new List<string[]>();

            CSVsplitToLines = CSVText[j].text.Split(rowChar);

            // Save the column names, assume first row.
            AlineSplitToColums = CSVsplitToLines[0].Split(columnChar);
            for (int i = 0; i < AlineSplitToColums.Length; i++)
            {
                AlineSplitToColums[i] = AlineSplitToColums[i].Replace(ret, "");
            }
            ColumnNames[j] = new List<string>(AlineSplitToColums);
            numColumns = ColumnNames[j].Count;

            for (int i = 1; i < CSVsplitToLines.Length; i++)
            {
                AlineSplitToColums = CSVsplitToLines[i].Split(columnChar);
                if (AlineSplitToColums.Length == numColumns)
                {

                    double[] rowValues = new double[AlineSplitToColums.Length];

                    for (int k = 0; k < AlineSplitToColums.Length; k++)
                    {
                        if (double.TryParse(AlineSplitToColums[k], out ReadValue))
                        {
                            rowValues[k] = ReadValue;
                        }
                        else
                        {
                            rowValues[k] = double.NaN;
                        }
                    }
                    CSVnumbers[j].Add(rowValues);
                    CSVtext[j].Add((string[])AlineSplitToColums.Clone());
                }
            }
        }
    }
    

}
