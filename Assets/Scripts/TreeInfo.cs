﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeClass  {
    public Vector3 position;

    public float genusVal, familyVal, commonVal;

    public string[] treeInfo;

    public AudioClip soundClip;

    public AudioSource attachedSource;

    public int soundIndex;

    public int age;

    public bool isPlaying;

    public Vector3 GUIposition;

    public float guiSize;

    public bool isWobbling = false;

    public TreeClass(string[] CSVData)
    {
        position = GPSManager.getUnityPosition(double.Parse(CSVData[1]), double.Parse(CSVData[0]));

        treeInfo = (string[])CSVData.Clone();

    }
    

}


public class TreeDot
{
    public float epoch;
    static float latestEpoch;
    public static long bucketCount;
    public long bucket;
    public static List<List<TreeDot>> bucketList = new List<List<TreeDot>>();
    public int speakerNo;
    public TreeClass tree;

    public static int ringCount;
    public int ringIndex;

    public static int largestRingIndex;

    public static int bucketInterval = 10;

    public static int ringLimit;

    public int bLI;
    public int tLI;



    public TreeDot(TreeClass tree, float epoch, int speakerNo)
    {
        this.tree = tree;
        this.epoch = epoch;
        this.speakerNo = speakerNo;

       if ((long)(epoch *bucketInterval) != (long)(latestEpoch *bucketInterval)) //if there is a new bucket
        {
            bucketCount++;
            
            this.bucket = bucketCount;

            ringCount = 1;
            this.ringIndex = ringCount;

            latestEpoch = epoch;

            bucketList.Add(new List<TreeDot>(){this});

        }

        else
        {
            //also make a new bucket if ringcount is larger than this
            if (ringCount > ringLimit)
            {
                bucketCount++;
                bucket = bucketCount;
                ringCount = 1;
                //ringIndex = ringCount;


                bucketList.Add(new List<TreeDot>() { this }); //create a new list of TreeDots with only this TreeDot in it
                
            }

            else //add this TreeDot to current bucket
            {
                bucket = bucketCount;
                ringCount++;
                //ringIndex = ringCount;

                if (ringCount > largestRingIndex)
                {
                    largestRingIndex = ringCount;

                    
                }

                bucketList[bucketList.Count - 1].Add(this); //add this TreeDot to the last list of TreeGrains;
            }

        }

        ringIndex = bucketList[bucketList.Count - 1].Count;

        bLI = bucketList.Count - 1;
        tLI = bucketList[bLI].Count - 1;
        
    }
}




public static class CustomTreeExtensions
{
    public static Vector3[] getPositions(this TreeClass[] tree, int[] indicies)
    {
        Vector3[] listOut = new Vector3[indicies.Length];
        for (int i = 0; i < indicies.Length; i++)
        {
            listOut[i] = tree[indicies[i]].position;
        }
        return listOut;
    }


    public static Vector3[] getAllPositions(this TreeClass[] tree)
    {
        Vector3[] listOut = new Vector3[tree.Length];
        for (int i = 0; i < tree.Length; i++)
        {
            listOut[i] = tree[i].position;
        }
        return listOut;
    }
}
