﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjNet;

public class CSVManager : MonoBehaviour {

    public TextAsset[] CSVFiles;
    CSVreader myCSVs;
    public static bool hasDigested = false;

    public static int shuffle = 12345;
    public string[] GridCSVHeadings;
    public string[] TreeCSVHeadings;
    public string[] TestDataCSVHeadings;
    public string[] testData;



    public GameObject gridObject;

    //static public Vector3[] GridLocation, TreeLocation, testPoints;
    //static public float[,] TreeAge;

    public int x;

    void OnValidate()
    {
        myCSVs = new CSVreader(CSVFiles);

        digestCSVFiles();

        GridCSVHeadings = myCSVs.ColumnNames[0].ToArray();
        TreeCSVHeadings = myCSVs.ColumnNames[1].ToArray();
        TestDataCSVHeadings = myCSVs.ColumnNames[3].ToArray();
        testData = Data.CommonNames.ToArray();
    }



    // Use this for initialization
    IEnumerator Start () {
        myCSVs = new CSVreader(CSVFiles);
        



        /*
        GridLocation = new Vector3[myCSVs.CSVData[0].Count]; //GridLocation is now an array of Vector3's of myCSVs.CSVData[0].Count length
        TreeLocation = new Vector3[myCSVs.CSVData[1].Count];
        testPoints = new Vector3[myCSVs.CSVData[3].Count];

        GridCSVHeadings = myCSVs.ColumnNames[0].ToArray();
        TreeCSVHeadings = myCSVs.ColumnNames[1].ToArray();
        TestDataCSVHeadings = myCSVs.ColumnNames[3].ToArray();


        TreeAge = new float[myCSVs.CSVData[1].Count, 2];
        */

        while (!GPSManager.isConnected)
        {
            yield return new WaitForSeconds(1);
        }
        digestCSVFiles();
    }


    void digestCSVFiles()
    {

        Data.grid = new HexGrid(myCSVs.CSVData[0], 25 / HexMetrics.RadiusChange);
        Data.treeList = new TreeClass[myCSVs.CSVText[1].Count];
        // Data.treeList = new TreeInfo(myCSVs.CSVText[1], myCSVs.CSVData[1]);
        for (int i = 0; i < myCSVs.CSVText[1].Count; i++)
        {
            Data.treeList[i] = new TreeClass(myCSVs.CSVText[1][i]);
            Data.grid.cells[(int)myCSVs.CSVData[1][i][23]].treeIndecies.Add(i);

            if (!Data.CommonNames.Contains(myCSVs.CSVText[1][i][4]))
            {
                Data.CommonNames.Add((myCSVs.CSVText[1][i][4]));
                Data.SuffledCommonNames.Add((myCSVs.CSVText[1][i][4]));
            }
            if (!Data.Family.Contains(myCSVs.CSVText[1][i][7]))
            {
                Data.Family.Add((myCSVs.CSVText[1][i][7]));
                Data.ShuffledFamily.Add((myCSVs.CSVText[1][i][7]));
            }
            if (!Data.Genus.Contains(myCSVs.CSVText[1][i][6]))
            {
                Data.Genus.Add((myCSVs.CSVText[1][i][6]));
                Data.ShuffledGenus.Add((myCSVs.CSVText[1][i][6]));
            }

        }
        for (int i = 0; i < myCSVs.CSVData[3].Count; i++)
        {
            Data.TestPathGPS.Add(new Vector3((float)myCSVs.CSVData[3][i][1], (float)myCSVs.CSVData[3][i][1], 0));
            Data.TestPathPositions.Add(GPSManager.getUnityPosition(myCSVs.CSVData[3][i][1], myCSVs.CSVData[3][i][1]));
        }

        //extract data and plop it on every tree

      Data.ShuffledGenus.Shuffle(10);
      Data.ShuffledFamily.Shuffle(10);
      Data.SuffledCommonNames.Shuffle(10);
       

        for (int i = 0; i < Data.treeList.Length; i++)
        {
            Data.treeList[i].genusVal = 1.0f * Data.ShuffledGenus.FindIndex(a => a == Data.treeList[i].treeInfo[6]) / Data.Genus.Count;
            Data.treeList[i].familyVal = 1.0f * Data.ShuffledFamily.FindIndex(a => a == Data.treeList[i].treeInfo[7]) / Data.Family.Count;
            Data.treeList[i].commonVal = 1.0f * Data.SuffledCommonNames.FindIndex(a => a == Data.treeList[i].treeInfo[4]) / Data.CommonNames.Count;

        }

        hasDigested = true;
    }
	
	// Update is called once per frame
	void Update () {
       // gridObject.transform.position = TreeLocation[x];

    }
}
