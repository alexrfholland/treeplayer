﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Data : MonoBehaviour
{

    public static HexGrid grid;


    public static TreeClass[] treeList;

    public GameObject player;

    public static Vector3 playerPosition;

    public static int currentHexIndex;
    public static List<int> neighbourHexIndex = new List<int>();
    public static List<TreeClass> currentTrees = new List<TreeClass>();
    public static List<TreeClass> neighbourTrees = new List<TreeClass>();
    public static List<TreeClass> surroundingTrees = new List<TreeClass>();

    static float disntantDistance = 200;
    public static List<TreeClass> distantTrees = new List<TreeClass>();

    public static List<string> CommonNames = new List<string>();
    public static List<string> Family = new List<string>();
    public static List<string> Genus = new List<string>();

    public static List<string> SuffledCommonNames = new List<string>();
    public static List<string> ShuffledFamily = new List<string>();
    public static List<string> ShuffledGenus = new List<string>();

    public static float visuliserDistance = 200;

    public static bool isOnHex = false;
    
    public static List<Vector3> TestPathPositions = new List<Vector3>();
    public static List<Vector3> TestPathGPS = new List<Vector3>();

    public bool usePath = false;

    // Use this for initialization
    IEnumerator Start()
    {

        while (!CSVManager.hasDigested)
        {
            yield return new WaitForSeconds(1);
        }

        if (player== null)
        {
            playerPosition = GPSManager.getAveragePostion;
        }
        else
        {
            playerPosition = player.transform.position;
        }

        findHexIndexSlow();
        findNeighbours();
        InvokeRepeating("findHexCell", 1, 0.01f);
        Invoke("istrue", 10f);
    }

    // Update is called once per frame
    void Update()
    {
            if (player == null)
            {
                playerPosition = GPSManager.getAveragePostion;
            }
            else
            {
                playerPosition = player.transform.position;
            }

    }

    void findHexIndexSlow() //what hex indicie am I standing on?
    {

        float minDist = float.MaxValue;
        int minIndex = -1;

        for (int i = 0; i < grid.cells.Count; i++)
        {
            if (Vector3.SqrMagnitude(playerPosition- grid.cells[i].CellPosition) < minDist)
            {
                minDist = Vector3.SqrMagnitude(playerPosition- grid.cells[i].CellPosition);
                minIndex = i;
            }
        }
        if (minIndex >= 0)
        {
            currentHexIndex = minIndex;
        }
    }

    void findNeighbours() //what are my neighbouring cells? add neighbouring cells into the neighbourIndex array, add trees in neighbouring cells  to neighbourTrees
    {
        float errorDistance = 5;
        float minDist = float.MaxValue;
        float tempDist;
        List<int> neighbourIndex = new List<int>();
        for (int i = 0; i < grid.cells.Count; i++)
        {
            tempDist = Vector3.SqrMagnitude(grid.cells[currentHexIndex].CellPosition- grid.cells[i].CellPosition);
            if (i != currentHexIndex && tempDist < minDist - errorDistance)
            {
                neighbourIndex.Clear();
                neighbourIndex.Add(i);
                minDist = tempDist;
            }
            else if (i != currentHexIndex &&  tempDist <= minDist + errorDistance)
            {
                neighbourIndex.Add(i);
            }
        }
        neighbourHexIndex = new List<int>(neighbourIndex);

        neighbourTrees.Clear();
        for (int i = 0; i < neighbourHexIndex.Count; i++)
        {
            for (int j = 0; j < grid.cells[neighbourHexIndex[i]].treeIndecies.Count; j++)
            {
                neighbourTrees.Add(treeList[grid.cells[neighbourHexIndex[i]].treeIndecies[j]]);
            }
        }

    }

    
    //update player indicie (currenthexIndex), redo neighbours if new cell indicie (
    /// <summary>
    /// This loops each of the surrounding hex cells, if one is closer than the current hex cell then it will update that hex cell and find new neighbours
    /// </summary>
    void findHexCell()
    {
        float minDist = Vector3.SqrMagnitude(playerPosition- grid.cells[currentHexIndex].CellPosition); //distance between me and centre of my cell
        float tempDist;
        bool updatePosition = false;
        

        if (neighbourHexIndex.Count < 4) //create neighbour cells if there are none
        {
            findNeighbours();
        }
        for (int i = 0; i < neighbourHexIndex.Count; i++)
        {
            tempDist = Vector3.SqrMagnitude(playerPosition- grid.cells[neighbourHexIndex[i]].CellPosition); //what is closer? my cell or this neighbour cell? if neighbour, sets  bool to update cell indice
            if (tempDist < minDist)
            {
                currentHexIndex = neighbourHexIndex[i];
                updatePosition = true;
            }
        }
        if (updatePosition)
        {
            currentTrees.Clear(); //start fresh
            for (int i = 0; i < grid.cells[currentHexIndex].treeIndecies.Count; i++)
            {
                currentTrees.Add(treeList[grid.cells[currentHexIndex].treeIndecies[i]]); //add new cells trees
            }
            findNeighbours();//what are my neighbouring cells? add neighbouring cells into the neighbourIndex array, add trees in neighbouring cells  to neighbourTrees
            updateDistantTrees(); //distant tree neighbours- within visualiser distance


            if (minDist < 100 * 100) // clear tree sound queue when 100 
                // play all tree sounds if within 100m to ensure at start of game distance cells do not fill up tree sound queue
            {
                soundManager.clearQueue();
                for (int i = 0; i < currentTrees.Count; i++)
                {
                    soundManager.addTreeSoundToQueue(currentTrees[i]); //add your cell's trees to sound queue
                }
                for (int i = 0; i < neighbourTrees.Count; i++)
                {
                    if (Vector3.Distance(playerPosition, neighbourTrees[i].position) < 25) // add neighbouring trees within 25m of me to sound queue
                    {
                        soundManager.addTreeSoundToQueue(neighbourTrees[i]);
                    }
                }
            }
        }
    }

    void updateDistantTrees()
    {
        distantTrees.Clear();
        for (int i = 0; i < treeList.Length; i++)
        {
            if (visuliserDistance* visuliserDistance > Vector3.SqrMagnitude(grid.cellCentres[currentHexIndex] - treeList[i].position))
            {
                distantTrees.Add(treeList[i]);
            }
        }
    }

}
