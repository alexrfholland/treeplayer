﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PointsToDraw { DebugData, DebugDataAndPlayer, DebugPlayer, GUI, AllFamily, AllGenus, AllCommon, GuiTree, TreeRing}

public enum Classification { Family, Genus, CommonName}

public class testParticlePlacer : MonoBehaviour
{

    public ParticleSystem ParticleSystem;

    public PointsToDraw pointsToDraw;

    public Classification classification;

    public Camera mainCamera;

    List<float> TreeAge = new List<float>();

    [Range(0, 255)]
    public byte r, g, b;

    public int bucketInterval = 10;

    public float size;
    public int floorAge = 1;
    public int ceilingAge = 20;
    public int nullAge = 11;

    [Range (0.10f, 3.00f)]
    public float totalScalingFactor;

    [Range(1, 255)]
    public int trans;
    Color partColor;

    bool randomiseGUI = false;

    public float treeRingsize = 0.9f;
    public float heartWood;
    float change;

    public float bucketsPerDegree = 1;



    public int ringLimt = 30;

    //branch stuff

    float tempAngle;

    public int trunkLimit = 20;
    public float branchScalingFactor;
    int uBranchIndex;
    int branchIndex;
    float branchAngleOffset;

    float angleOffset;
    int aLargestRingIndex;
    int aRingIndex;

    public float vertOffset;

    public bool isReset;

    ParticleSystem.Particle[] arrParticles;

    public bool isMultibucket = false;
    public int bucketGroups = 1;

    int ringIndex;

    

    /*
         USAGE NOTES
          1. In the particle system disable EVERYTHING except for Renderer.
          
         */

    // Use this for initialization
    void Start()
    {
      // GPSManager.convMGA94(-37.814955, 144.964493, out east, out north);
       // test.transform.position = new Vector3((float)(east-CSVManager.originEast), 0, (float)(north-CSVManager.originNorth));


        //TreeAge = new List<float>(CSVManager.TreeAge);



    }




    private void LateUpdate()
    {
        TreeDot.bucketInterval = bucketInterval;
        TreeDot.ringLimit = ringLimt;

        /*
        if (heartWood <= minH)
        {
            change = 1;
        }
        if (heartWood >= maxH)
        {
            change = -1;
        }

        heartWood = heartWood + (0.1f * change);
        */

            if (CSVManager.hasDigested)
        {
            List<Color32> colours = new List<Color32>();
            List<float> sizes = new List<float>();
            List<Vector3[]> positions = new List<Vector3[]>();
            
            switch (pointsToDraw)
            {
                case PointsToDraw.DebugData:
                    positions.Add(Data.grid.cellCentres);
                    colours.Add(new Color32(r, g, b, 255));
                    sizes.Add(size);

                    positions.Add(Data.treeList.getAllPositions());
                    colours.Add(new Color32(50, 255, 50, 255));
                    sizes.Add(size);

                    DrawMultipleListsParticles(positions, ParticleSystem, colours, sizes);
                    break;
                case PointsToDraw.DebugDataAndPlayer:
                    positions.Add(Data.grid.cellCentres);
                    colours.Add(new Color32(r, g, b, 255));
                    sizes.Add(size);

                    positions.Add(Data.treeList.getAllPositions());
                    colours.Add(new Color32(50, 255, 50, 255));
                    sizes.Add(size/2);

                    positions.Add(Data.grid.getPositions(Data.neighbourHexIndex.ToArray()));
                    colours.Add(new Color32(0, 255, 0, 255));
                    sizes.Add(size * 2);

                    positions.Add(new Vector3[] { Data.grid.cellCentres[Data.currentHexIndex] });
                    colours.Add(new Color32(255, 0, 0, 255));
                    sizes.Add(size * 3);

                    positions.Add(Data.neighbourTrees.ToArray().getAllPositions());
                    colours.Add(new Color32(255, 0, 255, 255));
                    sizes.Add(size * 2);

                    DrawMultipleListsParticles(positions, ParticleSystem, colours, sizes);
                    break;
                case PointsToDraw.DebugPlayer:
                    // Distant trees
                    positions.Add(Data.distantTrees.ToArray().getAllPositions());
                    colours.Add(new Color32(255, 255, 255, 255));
                    sizes.Add(size/2);

                    // Hex surrounding
                    positions.Add(Data.grid.getPositions(Data.neighbourHexIndex.ToArray()));
                    colours.Add(new Color32(50, 50, 255, 255));
                    sizes.Add(size * 2);

                    // Hec cell
                    positions.Add(new Vector3[] { Data.grid.cellCentres[Data.currentHexIndex] });
                    colours.Add(new Color32(255, 0, 0, 255));
                    sizes.Add(size * 2);

                    // Surroudning trees
                    positions.Add(Data.neighbourTrees.ToArray().getAllPositions());
                    colours.Add(new Color32(50, 255, 50, 255));
                    sizes.Add(size );

                    // current trees
                    positions.Add(Data.currentTrees.ToArray().getAllPositions());
                    colours.Add(new Color32(25, 255, 25, 255));
                    sizes.Add(size * 2);

                    DrawMultipleListsParticles(positions, ParticleSystem, colours, sizes);
                    break;
                case PointsToDraw.GUI:

                    positions.Add(Data.distantTrees.ToArray().getAllPositions());
                    colours.Add(new Color32(255, 255, 255, 255));
                    sizes.Add(size / 2);


                    DrawGUI(soundManager.playedTreeQueue.ToArray(), positions, ParticleSystem, colours, sizes);
                    break;

                case PointsToDraw.AllGenus:
                    DrawGUI(Data.treeList, ParticleSystem, size, pointsToDraw);
                    break;

                case PointsToDraw.AllFamily:
                    DrawGUI(Data.treeList, ParticleSystem, size, pointsToDraw);
                    break;

                case PointsToDraw.AllCommon:
                    DrawGUI(Data.treeList, ParticleSystem, size, pointsToDraw);
                    break;

                case PointsToDraw.GuiTree:

                    DrawGUI(Data.distantTrees.ToArray(), ParticleSystem, new Color32(255, 255, 255, 255), size);
                    break;

                case PointsToDraw.TreeRing:

                    DrawGUI(soundManager.visTreeGrain, ParticleSystem, size, pointsToDraw, 1);
                    break;

                default:
                    break;
                

                
            }
        }
        // DrawParticles(GridObjects, ParticleSystem, new Color32(255, 255, 255, 255),10f);
        // DrawParticles(TreeObjects, ParticleSystem, new Color32(r, g, b, 255), size);
        ///  DrawParticles(TreeObjectsEN, ParticleSystem, new Color32(r, g, b, 255), size);
    }


    static IEnumerator wobble(TreeClass tree)
    {
        float mod = 0.1f;
        for (int i = 0; i < 30 ; i++)
        {
            tree.guiSize -= mod;
            yield return new WaitForEndOfFrame();
        }
        for (int i = 0; i < 30 ; i++)
        {
            tree.guiSize += mod;
            yield return new WaitForEndOfFrame();
        }
        tree.isWobbling = false;
    }

    //treesnearby
    void DrawGUI(TreeClass[] treesToDraw, ParticleSystem particleSys, Color32 colour, float size)
    {
        ParticleSystem.Particle[] arrParticles;

        float w = (float)mainCamera.pixelWidth;
        float h = (float)mainCamera.pixelHeight;
        float scaledDist;
        // Create a new array to hold the particles we expect to display from the particle system. In this case use the listObjects count
        //  to tell how many particles are needed
        int nParticles = treesToDraw.Length;
        

        arrParticles = new ParticleSystem.Particle[nParticles];
        //arrParticles = new ParticleSystem.Particle[CSV.dataMap.Count];

        // Critical step! You MUST fire GetParticles and pass the array you just created into GetParticles
        //  This gets a generic set of particles to modify and set BACK to the system once completed
        particleSys.GetParticles(arrParticles);
        // Run a loop through the number of particles.. i is our counter value
        


        for (int i = 0; i < treesToDraw.Length; i++)
        {
            int age; float normalisedAge;
            if (int.TryParse(treesToDraw[i].treeInfo[9], out age))
            {
                age = 2017 - age;
            }
            else
            {
                age = 1;
            }
            normalisedAge = ((float)age) / 117;
            colour = Color.HSVToRGB(0.3f, normalisedAge, 0.8f);

            scaledDist = Vector3.Distance(Data.playerPosition, treesToDraw[i].position) / Data.visuliserDistance;
            // For ease of use assign a ParticleSystem.particle for the current object
            ParticleSystem.Particle par = arrParticles[i];
            if (treesToDraw[i].GUIposition.sqrMagnitude == 0)
            {
                treesToDraw[i].GUIposition = new Vector3(Random.Range(0, w ), Random.Range(0, h), 350);

                //treesToDraw[i].GUIposition = new Vector3(Random.Range(w/2-(w/2)*(scaledDist), w / 2 + (w / 2) * (scaledDist)), Random.Range(h / 2 - (h / 2) * (scaledDist), h / 2 + (h / 2) * (scaledDist)), 350);
            }



            if (treesToDraw[i].isPlaying)
            {
                if (!treesToDraw[i].isWobbling)
                {
                    treesToDraw[i].guiSize = size * 5 * normalisedAge + 4;
                    StartCoroutine(wobble(treesToDraw[i]));
                    treesToDraw[i].isWobbling = true;
                }

                par.startSize = treesToDraw[i].guiSize;
                par.startColor = colour; //new Color32(50, 255, 50, 255);//  par.startColor = colour;
                if (randomiseGUI)
                {
                    par.position = mainCamera.ScreenToWorldPoint(treesToDraw[i].GUIposition);
                }
                else
                {
                    par.position = treesToDraw[i].position;
                }
              // 
            }
            else if (Vector3.Distance(Data.playerPosition , treesToDraw[i].position) < 200)
            {
                par.startSize = size * 0.1f;
                par.startColor = new Color32(255, 255, 255, 255);//  par.startColor = colour;
                if (randomiseGUI)
                {
                    par.position = mainCamera.ScreenToWorldPoint(treesToDraw[i].GUIposition);
                }
                else
                {
                    par.position = treesToDraw[i].position;
                }
            }
            par.velocity = Vector3.zero;
            par.remainingLifetime = 900f;


            // Critical step! You MUST assign the modified particle BACK into the current position of the particles
            //  This seems to be how the data is saved to the particle.
            arrParticles[ i] = par;
        }


        // Apply the particle changes to the particle system
        //  Note there is no need for Emit or Play here. Execute SetParticles and it works.
        particleSys.SetParticles(arrParticles, arrParticles.Length);

       
    }

    //debug
    void DrawGUI(TreeClass[] treesToDraw, List<Vector3[]> positions, ParticleSystem particleSys, List<Color32> colour, List<float> size)
    {
        
        int count = 0;
        for (int i = 0; i < treesToDraw.Length; i++)
        {
            if (treesToDraw[i].isPlaying)
            {
                count++;
            }
        }

        // Create a new array to hold the particles we expect to display from the particle system. In this case use the listObjects count
        //  to tell how many particles are needed
        int nParticles = 0;
        for (int i = 0; i < positions.Count; i++)
        {
            nParticles += positions[i].Length;
        }
        nParticles += treesToDraw.Length;

        arrParticles = new ParticleSystem.Particle[nParticles];
        //arrParticles = new ParticleSystem.Particle[CSV.dataMap.Count];

        // Critical step! You MUST fire GetParticles and pass the array you just created into GetParticles
        //  This gets a generic set of particles to modify and set BACK to the system once completed
        particleSys.GetParticles(arrParticles);
        // Run a loop through the number of particles.. i is our counter value
        int startIndex = 0;
        for (int i = 0; i < positions.Count; i++)
        {
            for (int j = 0; j < positions[i].Length; j++)
            {
                // For ease of use assign a ParticleSystem.particle for the current object
                ParticleSystem.Particle par = arrParticles[startIndex + j];

                par.startSize = size[i];
                par.startColor = colour[i];

                //  par.startColor = colour;
                par.position = positions[i][j];
                par.velocity = Vector3.zero;
                par.remainingLifetime = 900f;


                // Critical step! You MUST assign the modified particle BACK into the current position of the particles
                //  This seems to be how the data is saved to the particle.
                arrParticles[startIndex + j] = par;
            }
            startIndex += positions[i].Length;
        }


        for (int i = 0; i < treesToDraw.Length; i++)
        {
            // For ease of use assign a ParticleSystem.particle for the current object
            ParticleSystem.Particle par = arrParticles[startIndex + count - 1];

            if (treesToDraw[i].GUIposition.sqrMagnitude == 0)
            {
                // treesToDraw[i].GUIposition = mainCamera.ScreenToWorldPoint(new Vector3(Random.Range(0, (float)mainCamera.pixelWidth), Random.Range(0, (float)mainCamera.pixelWidth), mainCamera.nearClipPlane));
            }


            par.startSize = size[0] * 2;

            if (treesToDraw[i].isPlaying)
            {
                par.startColor = new Color32(0, 255, 0, 255);//  par.startColor = colour;
            }
            else
            {
                par.startColor = new Color32(255, 0, 0, 255);//  par.startColor = colour;
            }
            par.position = treesToDraw[i].position;
            par.velocity = Vector3.zero;
            par.remainingLifetime = 900f;


            // Critical step! You MUST assign the modified particle BACK into the current position of the particles
            //  This seems to be how the data is saved to the particle.
            arrParticles[startIndex + i] = par;
            count--;
        }


        // Apply the particle changes to the particle system
        //  Note there is no need for Emit or Play here. Execute SetParticles and it works.
        particleSys.SetParticles(arrParticles, arrParticles.Length);
    }

    //show all trees
    void DrawGUI(TreeClass[] treesToDraw, ParticleSystem particleSys, float size, PointsToDraw trees)
    {
        ParticleSystem.Particle[] arrParticles;

        Color32 colour;
        float w = (float)mainCamera.pixelWidth;
        float h = (float)mainCamera.pixelHeight;
        float scaledDist;
        // Create a new array to hold the particles we expect to display from the particle system. In this case use the listObjects count
        //  to tell how many particles are needed
        int nParticles = treesToDraw.Length;


        arrParticles = new ParticleSystem.Particle[nParticles];
        //arrParticles = new ParticleSystem.Particle[CSV.dataMap.Count];

        // Critical step! You MUST fire GetParticles and pass the array you just created into GetParticles
        //  This gets a generic set of particles to modify and set BACK to the system once completed
        particleSys.GetParticles(arrParticles);
        // Run a loop through the number of particles.. i is our counter value



        for (int i = 0; i < treesToDraw.Length; i++)
        {
            int age; float normalisedAge;
            if (int.TryParse(treesToDraw[i].treeInfo[9], out age))
            {
                age = 2017 - age;
            }
            else
            {
                age = 1;
            }
            normalisedAge = ((float)age) / 117;

            if (trees == PointsToDraw.AllGenus)
            {
                colour = Color.HSVToRGB(treesToDraw[i].genusVal, normalisedAge + 0.5f, 1);
            }

            else if (trees == PointsToDraw.AllFamily)
            {
                colour = Color.HSVToRGB(treesToDraw[i].familyVal, normalisedAge + 0.5f, 1);
            }

            else //commonname
            {
                colour = Color.HSVToRGB(treesToDraw[i].commonVal, normalisedAge + 0.5f, 1);
            }

            scaledDist = Vector3.Distance(Data.playerPosition, treesToDraw[i].position) / Data.visuliserDistance;
            // For ease of use assign a ParticleSystem.particle for the current object
            ParticleSystem.Particle par = arrParticles[i];
            par.startSize = size * 1;
            par.startColor = colour;
            par.position = treesToDraw[i].position;

            

            par.velocity = Vector3.zero;
            par.remainingLifetime = 900f;


            // Critical step! You MUST assign the modified particle BACK into the current position of the particles
            //  This seems to be how the data is saved to the particle.
            arrParticles[i] = par;
        }


        // Apply the particle changes to the particle system
        //  Note there is no need for Emit or Play here. Execute SetParticles and it works.
        particleSys.SetParticles(arrParticles, arrParticles.Length);
    }

    //TreeRings
    void DrawGUI(List<TreeDot> treesToDraw, ParticleSystem particleSys, float size, PointsToDraw trees, int check)
    {
        ParticleSystem.Particle[] arrParticles;

        Color32 colour;
        float w = (float)mainCamera.pixelWidth;
        float h = (float)mainCamera.pixelHeight;
        float scaledDist;
        // Create a new array to hold the particles we expect to display from the particle system. In this case use the listObjects count
        //  to tell how many particles are needed
        int nParticles = treesToDraw.Count;


        arrParticles = new ParticleSystem.Particle[nParticles];
        //arrParticles = new ParticleSystem.Particle[CSV.dataMap.Count];

        // Critical step! You MUST fire GetParticles and pass the array you just created into GetParticles
        //  This gets a generic set of particles to modify and set BACK to the system once completed
        particleSys.GetParticles(arrParticles);
        // Run a loop through the number of particles.. i is our counter value
        Vector3 origin;
        float angle;
        float radius;
        float xpos;
        float ypos;

        

        Vector3 screenPos;

        for (int i = 0; i < treesToDraw.Count; i++)
        {
            int age; float normalisedAge;
            if (int.TryParse(treesToDraw[i].tree.treeInfo[9], out age))
            {
                age = 2017 - age; // convert year planted to age of treee

                if (age > ceilingAge) // cap largest age
                {
                    age = ceilingAge;
                }

                if (age < floorAge) // floor smallest age
                {
                    age = floorAge;
                }
            }

            else
            {
                age = nullAge; //assumed age for blanks
            }

            normalisedAge = ((float)age) / 118; //convert to value from 0 to 1




            Debug.Log(normalisedAge);


            switch (classification)
            {
                case Classification.CommonName:
                    colour = Color.HSVToRGB(treesToDraw[i].tree.commonVal, 1, 1);
                    break;

                case Classification.Family:
                    colour = Color.HSVToRGB(treesToDraw[i].tree.familyVal, 1, 1);
                    break;

                default:
                    colour = Color.HSVToRGB(treesToDraw[i].tree.genusVal, 1, 1);
                    break;
            }


            // For ease of use assign a ParticleSystem.particle for the current object
            ParticleSystem.Particle par = arrParticles[i];
            par.startSize = size * normalisedAge * totalScalingFactor;
            par.startColor = new Color32(colour.r, colour.g, colour.b, (byte)trans);
            TreeDot thisTree = treesToDraw[i];

            
           
            int bLI = thisTree.bLI;
            int tLI = thisTree.tLI;

            for (int j = 0; j <  (bLI % bucketGroups); j++)
            {
                tLI = tLI + TreeDot.bucketList[bLI - (j+1)].Count;
            }

            ringIndex = tLI + 1;


            tempAngle = (float)(bLI - (bLI % bucketGroups)) / TreeDot.bucketCount * 2 * Mathf.PI;

            

           /*
            int bLI = thisTree.bLI;
            int tLI = thisTree.tLI;

            
            if (isMultibucket && bLI % 2 == 1)
            {
                    ringIndex = tLI + 1 + TreeDot.bucketList[bLI - 1].Count;
                        
                    tempAngle = ((float)bLI -1) / TreeDot.bucketList.Count * 2 * Mathf.PI;
   
            }

      
            else
            { 
                ringIndex = tLI + 1;
                tempAngle = (float)(bLI) / TreeDot.bucketList.Count * 2 * Mathf.PI;
            }
            
            */


            //branching stuff
            uBranchIndex = ringIndex - trunkLimit;
        branchIndex = uBranchIndex / 2;

       // tempAngle = (treesToDraw[i].bucket) / (float)TreeDot.bucketCount * 2 * Mathf.PI;


         //treeDot is on branch
         branchAngleOffset =  360f / TreeDot.bucketCount / bucketGroups * 2 * Mathf.PI / 2 * branchScalingFactor;

            if (uBranchIndex > 0)
            {
                if (uBranchIndex % 2 == 0)
                {
                    angleOffset = branchAngleOffset;
                }

                if (uBranchIndex % 2 == 1)
                {
                    angleOffset = -1 * branchAngleOffset;
                }

                aRingIndex = branchIndex + trunkLimit;
            }


            //treeDot is on trunk
            else
            {
                angleOffset = 0;
                aRingIndex = ringIndex;
            }


                //calc angle
                angle = tempAngle + angleOffset;

                //calc radius
                if (aRingIndex > aLargestRingIndex)
                {
                aLargestRingIndex = aRingIndex;
                }

                radius = (aRingIndex + heartWood) / (aLargestRingIndex + heartWood) * w / 2 * treeRingsize * totalScalingFactor;

            
          

            xpos = radius * Mathf.Cos(angle);
            ypos = radius * Mathf.Sin(angle) + vertOffset;

            screenPos = new Vector3(xpos + w / 2, ypos + h / 2, 100);
            
            par.position = mainCamera.ScreenToWorldPoint(screenPos);


            par.velocity = Vector3.zero;
            par.remainingLifetime = 900f;
       
           

            // Critical step! You MUST assign the modified particle BACK into the current position of the particles
            //  This seems to be how the data is saved to the particle.
            arrParticles[i] = par;
        }

   
        // Apply the particle changes to the particle system
        //  Note there is no need for Emit or Play here. Execute SetParticles and it works.
        particleSys.SetParticles(arrParticles, arrParticles.Length);

        if (isReset)
        {
            treesToDraw.Clear();
            soundManager.isReset = true;
            TreeDot.bucketCount = 0;
            TreeDot.largestRingIndex = 0;

            isReset = false;
        }
    }



    void DrawMultipleListsParticles(List<Vector3[]> positions, ParticleSystem particleSys, List<Color32> colour, List<float> size)
    {
        ParticleSystem.Particle[] arrParticles;


        // Create a new array to hold the particles we expect to display from the particle system. In this case use the listObjects count
        //  to tell how many particles are needed
        int nParticles = 0;
        for (int i = 0; i < positions.Count; i++)
        {
            nParticles += positions[i].Length;
        }

        arrParticles = new ParticleSystem.Particle[nParticles];
        //arrParticles = new ParticleSystem.Particle[CSV.dataMap.Count];

        // Critical step! You MUST fire GetParticles and pass the array you just created into GetParticles
        //  This gets a generic set of particles to modify and set BACK to the system once completed
        particleSys.GetParticles(arrParticles);
        int startIndex = 0;
        // Run a loop through the number of particles.. i is our counter value
        for (int i = 0; i < positions.Count; i++)
        {
            for (int j = 0; j < positions[i].Length; j++)
            {
                // For ease of use assign a ParticleSystem.particle for the current object
                ParticleSystem.Particle par = arrParticles[startIndex + j];

                par.startSize = size[i];
                par.startColor = colour[i];

                //  par.startColor = colour;
                par.position = positions[i][j];
                par.velocity = Vector3.zero;
                par.remainingLifetime = 900f;


                // Critical step! You MUST assign the modified particle BACK into the current position of the particles
                //  This seems to be how the data is saved to the particle.
                arrParticles[startIndex + j] = par;
            }
            startIndex += positions[i].Length;
        }

        // Apply the particle changes to the particle system
        //  Note there is no need for Emit or Play here. Execute SetParticles and it works.
        particleSys.SetParticles(arrParticles, arrParticles.Length);
    }

    void DrawOneListParticles(Vector3[] positions, ParticleSystem particleSys, Color32 colour, float size)
    {
        ParticleSystem.Particle[] arrParticles;


        // Create a new array to hold the particles we expect to display from the particle system. In this case use the listObjects count
        //  to tell how many particles are needed

        arrParticles = new ParticleSystem.Particle[positions.Length];
        //arrParticles = new ParticleSystem.Particle[CSV.dataMap.Count];

        // Critical step! You MUST fire GetParticles and pass the array you just created into GetParticles
        //  This gets a generic set of particles to modify and set BACK to the system once completed
        particleSys.GetParticles(arrParticles);

        // Run a loop through the number of particles.. i is our counter value
        for (int i = 0; i < arrParticles.Length; i++)
        {

            // For ease of use assign a ParticleSystem.particle for the current object
            ParticleSystem.Particle par = arrParticles[i];

            /*
            float age = 2017 - CSVManager.TreeAge[i, 0];
            float lifeEx = CSVManager.TreeAge[i, 1];

            colour = Color.HSVToRGB(1, 1, 1);

            if (age > 0)
            {
                par.startSize = 10;
                par.startColor = colour;
            }
            else
            {
                par.startSize = size;
                par.startColor = new Color32(50, 30, 80, 255);
            }
            */
            if (i == Data.currentHexIndex)
            {
                par.startSize = size * 3;
                par.startColor = new Color32(255, 0, 0, 255);
            }
            else if (Data.neighbourHexIndex.Contains(i))
            {

                par.startSize = size * 2;
                par.startColor = new Color32(0, 255, 0, 255);
            }
            else
            {
                par.startSize = size;
                par.startColor = colour;
            }

            //  par.startColor = colour;
            par.position = positions[i];
            par.velocity = Vector3.zero;
            par.remainingLifetime = 900f;


            // Critical step! You MUST assign the modified particle BACK into the current position of the particles
            //  This seems to be how the data is saved to the particle.
            arrParticles[i] = par;
        }

        // Apply the particle changes to the particle system
        //  Note there is no need for Emit or Play here. Execute SetParticles and it works.
        particleSys.SetParticles(arrParticles, arrParticles.Length);


    }

    /*
	void GenerateObjects() {

        //listObjects should be populated by whatever means you need - DB, array, text file et
        //listObjects = CAPos;
        listObjects = CSV.dataMap;
        int i = 0;

		theParticleSystem.Clear ();
		theParticleSystem.maxParticles = listObjects.Count;

		foreach (Vector3 obj in listObjects) 
		{
			// Get the X Y and Z coordinates for the object
			float x = obj.x * fDistanceModifier;
            float y = obj.y * fDistanceModifier;
			float z = obj.z * fDistanceModifier;

			// Set the position to a vector3 
			Vector3 objPos = new Vector3 (x,y,z);

			if(showDebugMessages)
				Debug.Log("Position of obj: " + objPos.ToString());

			// Create the game objects that sit at the positions in space where objs exist
			//  Set the position and parent the object accordingly
			//clone.name = obj.name;
			Transform clone = (Transform)Instantiate(objPrefab, objPos, Quaternion.identity);

			clone.SetParent(gameObject.transform);

			i++;
		}

		if(showDebugMessages)
			Debug.Log("Generate GenerateObjects has finished.");
	}

    void whispMovement()
	{
        float offset;
        int i = 0;

        foreach (Vector3 cell in LerpPos)
		{
			offset = Random.Range(-100,100);
			if (listObjects[i].y < CSV.dataMap[i].y)
			{
			LerpPos[i] = new Vector3(cell.x + (offset / 100), cell.y + Mathf.Abs(offset / 100), cell.z + (offset / 100));	
			}
			else
			{
LerpPos[i] = new Vector3(cell.x + (offset / 100), cell.y + (offset / 100), cell.z + (offset / 100));
			}
			
            i++;
        }  
    }
    */
}


public static class ExtensionMethods {
 
public static float Remap (this float value, float from1, float to1, float from2, float to2) {
    return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
}

    public static void Shuffle<T>(this IList<T> list, int seed)
    {
        var rng = new System.Random(seed);
        int n = list.Count;

        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

}